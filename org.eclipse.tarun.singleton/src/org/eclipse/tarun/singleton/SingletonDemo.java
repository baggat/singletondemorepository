package org.eclipse.tarun.singleton;

public class SingletonDemo {
	  
    // Step 1: private static variable of INSTANCE variable
    private static volatile SingletonDemo 
            INSTANCE;
  
    // Step 2: private constructor
    private SingletonDemo() {
  
    }
  
    // Step 3: Provide public static getInstance() method 
    // returning INSTANCE after checking
    public static SingletonDemo getInstance() {
  
        // double-checking lock
        if(null == INSTANCE){
  
            // synchronized block
            synchronized (SingletonDemo.class) {
                if(null == INSTANCE){
                    INSTANCE = new SingletonDemo();
                }
            }
        }
        return INSTANCE;
    }
}

