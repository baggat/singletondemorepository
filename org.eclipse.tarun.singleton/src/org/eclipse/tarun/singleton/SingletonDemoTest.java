package org.eclipse.tarun.singleton;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Assert;

import org.junit.jupiter.api.Test;

class SingletonDemoTest {

	@Test
public void TestSingletonObject() {
		
		SingletonDemo instance1 = SingletonDemo.getInstance();
		SingletonDemo instance2 = SingletonDemo.getInstance();
		
		Assert.assertSame("2 objects are same",instance1,instance2);
	}

}
